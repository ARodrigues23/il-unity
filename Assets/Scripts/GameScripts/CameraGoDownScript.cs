﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGoDownScript : MonoBehaviour {

	private TestCam camScript;

	private bool change;
	private float time;

	// Use this for initialization
	void Start () {
		camScript = GameObject.Find ("Main Camera").GetComponent<TestCam> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(change == true){
			if(camScript.HeightCameralvl >= -1f){
				camScript.HeightCameralvl -= 0.04f;
			}

		}
		else if (change == false){
			if(camScript.HeightCameralvl <= 4.5f){
				camScript.HeightCameralvl += 0.02f;
			}
		}
		if(time >= 0){
			time -= Time.deltaTime;
		}
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if(time <= 0.5f){
				time = 3;
				change = !change;
			}

		}
	}
}
