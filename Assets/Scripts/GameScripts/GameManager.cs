﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	[SerializeField]
	private WalkerManager walkerM;

	private LevelManager levelManager;

	private Animator anim;
	private Animator anim1;

	private PlayerControllerYin player;

	private PlayerControllerYang player2;

	private Transform yinPlayer;
	private Transform yangPlayer;


	private Vector2 inicialCheckPoint;
	private Vector2 checkPoint;

	public Vector2 CheckPoint {
		get {
			return checkPoint;
		}
		set {
			checkPoint = value;
		}
	}

	private bool death;

	public bool Death {
		get {
			return death;
		}
		set {
			death = value;
		}
	}

	private bool fadeOut;
	private bool fadeIn;
	[SerializeField]
	private Image blackScreen;

	private WoodCutScenesScript woodCutsceneScript;
	private bool dontMove;

	public bool DontMove {
		get {
			return dontMove;
		}
	}

	// Use this for initialization
	void Start () {
		
		levelManager = FindObjectOfType<LevelManager> ();

		player = FindObjectOfType<PlayerControllerYin> ();
		player2 = FindObjectOfType<PlayerControllerYang> ();

		anim = player.GetComponent<Animator> ();
		anim1 = player2.GetComponent<Animator> ();
	
		yinPlayer = GameObject.Find ("YinPlayer").GetComponent<Transform> ();
		yangPlayer = GameObject.Find ("YangPlayer").GetComponent<Transform> ();

		woodCutsceneScript = GameObject.Find("Main Camera").GetComponent<WoodCutScenesScript> ();


		inicialCheckPoint = new Vector2 (yinPlayer.transform.position.x, yinPlayer.transform.position.y);
		checkPoint = inicialCheckPoint;
		StartCoroutine ("StartLoad",0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(death == true){
			StartCoroutine("Restart");
		}

		Scene currentScene = SceneManager.GetActiveScene ();
		string sceneName = currentScene.name;

		if (woodCutsceneScript.PlayingCutscene == true && sceneName == "WoodWorld") {
			dontMove = true;
		} else
			dontMove = false;

		if(Input.GetKey(KeyCode.Escape)){
			Application.Quit();
		}

	}

	void FixedUpdate(){
		FadeIn ();
		FadeOut ();
	}

	void FadeIn(){
		if(fadeIn == true){
			blackScreen.color = new Color (0f, 0f, 0f, blackScreen.color.a - 0.02f);

			if(blackScreen.color.a <= 0f){
				blackScreen.color = new Color (0f,0f,0f,0f);
			}
		}

	}
	void FadeOut(){
		if(fadeOut == true){
			blackScreen.color = new Color (0f, 0f, 0f, blackScreen.color.a + 0.02f);

			if(blackScreen.color.a >= 1f){
				blackScreen.color = new Color (0f,0f,0f,1f);
			}
		}

	}

	IEnumerator StartLoad(){
		yield return new WaitForSeconds (1.5f);
		fadeIn = true;
		yield return new WaitForSeconds (1f);
		fadeIn = false;
	}

	IEnumerator Restart()
	{
		fadeIn = false;
		fadeOut = true;
		player.GravityScaleValue = 0;
		player2.GravityScaleValue = 0;
		yield return new WaitForSeconds (1f);
		yinPlayer.transform.position = checkPoint;
		yangPlayer.transform.position = checkPoint;

		//GameObject.Find("Level Manager").GetComponent<FadeIn>().BeginFade(1);
		//yield return new WaitForSeconds(2);
		//GameObject.Find("Level Manager").GetComponent<FadeIn>().BeginFade(-1);
		//levelManager.RespawnPlayer();
		death = false;
		fadeOut = false;

		yield return new WaitForSeconds (1f);
		fadeIn = true;
		player.GravityScaleValue = 3;
		player2.GravityScaleValue = 3;
		yield return new WaitForSeconds (1f);
		fadeIn = false;

		//anim.SetBool ("Death", death);
		//anim1.SetBool ("Death1", death);


		//Application.LoadLevel(Application.loadedLevel);
		//levelManager.RespawnPlayer();

	}
}
