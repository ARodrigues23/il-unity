﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingBarScript : MonoBehaviour {	// Use this for initialization

	AsyncOperation ao;
	public GameObject loadingScreenBG;
	public Slider progBar;

	private bool isFakeLoadingBar = false;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadLevel01(){
		loadingScreenBG.SetActive (true);
		progBar.gameObject.SetActive (true);

		if (!isFakeLoadingBar) {
			StartCoroutine ("LoadLevelWithRealProgress");
		} 
		else 
		{
		
		}
	}

	IEnumerator LoadLevelWithRealProgress()
	{
		yield return new WaitForSeconds (1);
		ao = SceneManager.LoadSceneAsync (3);
		ao.allowSceneActivation = false;

		while(!ao.isDone)
		{
			progBar.value = ao.progress;

			if(ao.progress == 0.9f)
			{
				progBar.value = 1f;
				yield return new WaitForSeconds (2);
				ao.allowSceneActivation = true;
			}
			yield return null;
		}
	}
}
