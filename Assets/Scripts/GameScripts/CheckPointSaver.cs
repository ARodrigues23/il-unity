﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointSaver : MonoBehaviour {

	private GameManager gm;

	// Use this for initialization
	void Start () {
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {

			gm.CheckPoint = new Vector2 (transform.position.x, transform.position.y);
		}

	}
}
