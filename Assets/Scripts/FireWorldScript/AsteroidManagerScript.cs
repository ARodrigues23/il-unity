﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManagerScript : MonoBehaviour {

	private bool asteroidDownStyle;

	[SerializeField]
	private GameObject asteroid;

	[SerializeField]
	private GameObject asteroid2;

	[SerializeField]
	private GameObject asteroidDown;

	private TriggerFallCutsceneScript triggerScript;
	private CristalBossManagerScript cristalScript;

	[SerializeField]
	private Transform pos1;
	[SerializeField]
	private Transform pos2;
	[SerializeField]
	private Transform pos3;
	[SerializeField]
	private Transform pos4;

	private float randomPos;
	private float randomAsteroid1;
	private float randomAsteroid2;
	private float randomAsteroid3;
	private float randomAsteroid4;

	[SerializeField]
	private float spawnTime;

	// Use this for initialization
	void Start () {
		cristalScript = GameObject.Find ("BossCristal").GetComponent<CristalBossManagerScript> ();
		triggerScript = GameObject.Find ("TriggerFallCutscene").GetComponent<TriggerFallCutsceneScript> ();
		asteroidDownStyle = true;
		InvokeRepeating ("DownFireAsteroid", 1f, 1f);
		InvokeRepeating ("FireAsteroid", 2.5f, 2.5f);

	}

	// Update is called once per frame
	void Update () {
		if(cristalScript.Phase2Start == true){
			asteroidDownStyle = false;
		}
	}

	void DownFireAsteroid(){
		if (triggerScript.StartCutscene == true) {
			if (asteroidDownStyle == true) {
				randomPos = Random.Range (1, 5);
				randomAsteroid1 = Random.Range (1, 3);
				randomAsteroid2 = Random.Range (1, 3);
				randomAsteroid3 = Random.Range (1, 3);
				randomAsteroid4 = Random.Range (1, 3);
				if (randomPos == 1) {

					Instantiate (asteroidDown, pos1.transform.position, Quaternion.identity);

				}
				if (randomPos == 2) {

					Instantiate (asteroidDown, pos2.transform.position, Quaternion.identity);

				}
				if (randomPos == 3) {

					Instantiate (asteroidDown, pos3.transform.position, Quaternion.identity);
				}
				if (randomPos == 4) {

					Instantiate (asteroidDown, pos4.transform.position, Quaternion.identity);
				}
			}


		}
	}

	void FireAsteroid(){
		if(triggerScript.StartCutscene == true){
			
			if (asteroidDownStyle == false && cristalScript.End == false){
				randomPos = Random.Range (1, 5);
				randomAsteroid1 = Random.Range (1, 3);
				randomAsteroid2 = Random.Range (1, 3);
				randomAsteroid3 = Random.Range (1, 3);
				randomAsteroid4 = Random.Range (1, 3);
				if(randomPos == 1){

					if(randomAsteroid1 == 1){
						Instantiate (asteroid2, pos1.transform.position, Quaternion.identity);
					}
					if(randomAsteroid1 == 2){
						Instantiate (asteroid2, pos1.transform.position, Quaternion.identity);
					}

				}
				if(randomPos == 2){

					if(randomAsteroid2 == 1){
						Instantiate (asteroid, pos2.transform.position, Quaternion.identity);
					}
					if(randomAsteroid2 == 2){
						Instantiate (asteroid2, pos2.transform.position, Quaternion.identity);
					}

				}
				if(randomPos == 3){

					if(randomAsteroid3 == 1){
						Instantiate (asteroid, pos3.transform.position, Quaternion.identity);
					}
					if(randomAsteroid3 == 2){
						Instantiate (asteroid2, pos3.transform.position, Quaternion.identity);
					}
				}
				if(randomPos == 4){

					if(randomAsteroid4 == 1){
						Instantiate (asteroid, pos4.transform.position, Quaternion.identity);
					}
					if(randomAsteroid4 == 2){
						Instantiate (asteroid2, pos4.transform.position, Quaternion.identity);
					}
				}
			}

		}
				
	}
}
