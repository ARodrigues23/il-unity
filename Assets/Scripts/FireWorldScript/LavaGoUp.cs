﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaGoUp : MonoBehaviour {

	private CristalBossManagerScript cristalScript;
	private GameManager gmScript;

	[SerializeField]
	private GameObject lava;

	private bool restartBetter;

	[SerializeField]
	private float speed;

	private Vector2 spawnPos;
	private float respawnTime;
	private bool respawnSet;

	private bool goUp;
	private bool notDead;

	// Use this for initialization
	void Start () {
		spawnPos = lava.transform.position;

		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		cristalScript = GameObject.Find ("BossCristal").GetComponent<CristalBossManagerScript> ();
		goUp = true;
	}
	
	// Update is called once per frame
	void Update () {



		if(cristalScript.Phase3Start == true && goUp == true && restartBetter == false){
			StartCoroutine ("TimeToGoUp1",0f);

		}
		if(restartBetter == true && goUp == true && cristalScript.End == false){
			StartCoroutine ("TimeToGoUp2",0f);
		}
		if(gmScript.Death == true){
			respawnSet = true;
			goUp = false;
			notDead = false;
			StopCoroutine ("TimeToGoUp1");
			StopCoroutine ("TimeToGoUp2");
		}
		if(respawnSet == true){
			respawnTime += Time.deltaTime;
			if(respawnTime >= 1.5f){
				lava.transform.position = spawnPos;
				respawnTime = 0;
				respawnSet = false;
			}
		}
		if(notDead == true && gmScript.Death == false){
			goUp = true;
		}

		if(cristalScript.End == true){
			lava.transform.position = spawnPos;
		}
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			notDead = true;

		}
	}

	IEnumerator TimeToGoUp1(){
		yield return new WaitForSeconds (5f);
		restartBetter = true;
		lava.transform.position = Vector3.MoveTowards (lava.transform.position, new Vector3(lava.transform.position.x,transform.position.y+30f,transform.position.z), speed * Time.deltaTime);
	}
	IEnumerator TimeToGoUp2(){
		yield return new WaitForSeconds (1.5f);
		lava.transform.position = Vector3.MoveTowards (lava.transform.position, new Vector3(lava.transform.position.x,transform.position.y+30f,transform.position.z), speed * Time.deltaTime);
	}
}
