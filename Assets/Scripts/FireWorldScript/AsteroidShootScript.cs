﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidShootScript : MonoBehaviour {

	private GameManager gmScript;


	private Transform yinPos;

	private Vector3 goToPos;

	private Vector3 curPos;
	private Vector3 lastPos;

	[SerializeField]
	private float speed;

	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		yinPos = GameObject.Find ("YinPlayer").GetComponent<Transform> ();
		goToPos = new Vector3 (yinPos.transform.position.x, yinPos.transform.position.y, yinPos.transform.position.z);
		Destroy (this.gameObject,15f);
	}
	
	// Update is called once per frame
	void Update () {

		//rb.AddForce (yangPos.transform.position * 3f);
		//rb.AddForceAtPosition (Vector2.down*3, yinPos.transform.position);
		//transform.position = Vector3.Lerp (transform.position, goToPos, 6f * Time.deltaTime);
		transform.position = Vector3.MoveTowards (transform.position, new Vector3(goToPos.x,goToPos.y-2f,goToPos.z), speed * Time.deltaTime);
		CheckStopMovement ();
	}

	void CheckStopMovement(){
		curPos = transform.position;
		if(curPos == lastPos){
			Destroy (this.gameObject);
		}
		lastPos = curPos;
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			gmScript.Death = true;
			Destroy (this.gameObject);

		}
		if (other.transform.tag == "OutWall") {
			Destroy (this.gameObject);

		}
	}
}
