﻿using UnityEngine;
using System.Collections;

public class Cut : MonoBehaviour {

    //Escolher camara, depois animator, mais Yin e yang.

	public TestCam camera;
	public Animator Cutscene;
	public bool animating;
	public Transform Yin, Yang;
	public bool OneTime;
	public string selectcut;
	public Canvas CutCanvas;


	// Use this for initialization


	void Start () {

		camera=GameObject.Find("Main Camera").GetComponent<TestCam> ();
        camera.enabled = true;
        //Cutscene.SetBool("IsReady", false);
        Cutscene.enabled = false;
		CutCanvas.enabled = false;
//		Yin = Yin.GetComponent<PlayerController> ();
		//Yang = Yang.GetComponent<Jump> ();
		//Yang = Yang.GetComponent<Runfast> ();




    }

    void OnTriggerEnter2D (Collider2D cut){

		if (cut.tag == "Player" && OneTime == false) {

			OneTime = true;
	
			StartCoroutine ("OnCollide");
			Cutscene.SetTrigger (selectcut);
			Yin.GetComponent<Animator> ().SetFloat ("speed", 0);

		}
	
	
	}

	IEnumerator OnCollide(){
	
		Time.timeScale = 0;
		camera.enabled = false;
        Cutscene.enabled = true;
		animating = true;
		Yang.GetComponent<PlayerController2> ().enabled = false;
		Yin.GetComponent<PlayerController> ().enabled = false;
		CutCanvas.enabled = true;




		yield return new WaitForSeconds(10);
		Time.timeScale = 1;
		camera.enabled = true;
		Cutscene.enabled = false;
		animating = false;
		Yang.GetComponent<PlayerController2> ().enabled = true;
		Yin.GetComponent<PlayerController> ().enabled = true;
		CutCanvas.enabled = false;




	
	}
	// Update is called once per frame
	void Update () {
	
	}
}
