﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFallCutsceneScript : MonoBehaviour {

	private CristalBossManagerScript cristalScript;

	private bool startCutscene;

	public bool StartCutscene {
		get {
			return startCutscene;
		}
	}

	private bool platsMove;
	private Vector3 disapearPlatPos;
	private bool disapearBool;
	private bool stopCouroutine;

	private bool phase2Up;
	private bool phase3Up;

	[SerializeField]
	private GameObject plat1,plat2,plat3,plat4,plat5,plat6,plat7,plat8,plat9,plat10,plat11,plat12,plat13;

	[SerializeField]
	private GameObject plat21,plat22,plat23,plat24,plat25,plat26,plat27,plat28,plat29,plat210,plat211,plat212;

	[SerializeField]
	private GameObject defensePlat1,defensePlat2,defensePlat3,defensePlat4,defensePlat5,defensePlat6;

	[SerializeField]
	private GameObject dfPos1,dfPos2,dfPos3,dfPos4,dfPos5,dfPos6;

	[SerializeField]
	private GameObject plat31,plat32,plat33,plat34,plat35,plat36,plat37,plat38,plat39,plat310,plat311,plat312,plat313;

	[SerializeField]
	private GameObject pos1,pos2,pos3,pos4,pos5,pos6,pos7,pos8,pos9,pos10,pos11,pos12,pos13;

	[SerializeField]
	private GameObject backPos1,backPos2,backPos3,backPos4,backPos5,backPos6,backPos7,backPos8,backPos9,backPos10,backPos11,backPos12,backPos13;

	[SerializeField]
	private GameObject phase2Pos1,phase2Pos2,phase2Pos3,phase2Pos4,phase2Pos5,phase2Pos6,phase2Pos7,phase2Pos8,phase2Pos9,phase2Pos10,phase2Pos11,phase2Pos12;

	[SerializeField]
	private GameObject back2Pos1,back2Pos2,back2Pos3,back2Pos4,back2Pos5,back2Pos6,back2Pos7,back2Pos8,back2Pos9,back2Pos10,back2Pos11,back2Pos12;

	[SerializeField]
	private GameObject phase3Pos1,phase3Pos2,phase3Pos3,phase3Pos4,phase3Pos5,phase3Pos6,phase3Pos7,phase3Pos8,phase3Pos9,phase3Pos10,phase3Pos11,phase3Pos12,phase3Pos13;

	[SerializeField]
	private GameObject back3Pos1,back3Pos2,back3Pos3,back3Pos4,back3Pos5,back3Pos6,back3Pos7,back3Pos8,back3Pos9,back3Pos10,back3Pos11,back3Pos12,back3Pos13;



	private Rigidbody2D fallRock;

	// Use this for initialization
	void Start () {
		
		cristalScript = GameObject.Find ("BossCristal").GetComponent<CristalBossManagerScript> ();
		fallRock = GameObject.Find ("BlockingFallingRock").GetComponent<Rigidbody2D> ();
		disapearPlatPos = plat1.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(startCutscene == true){
			if(stopCouroutine == false){
				StartCoroutine("BossCutscene",0f);
			}


		}
		MovePlataforms ();

	}

	void MovePlataforms(){
		if(platsMove == true){
			/////phase 1
			if (cristalScript.Phase2Start == false) {
				//subir primeira vez
				print ("subir primeira vez");
				plat1.transform.position = Vector3.Lerp (plat1.transform.position, pos1.transform.position, 0.03f);
				plat2.transform.position = Vector3.Lerp (plat2.transform.position, pos2.transform.position, 0.035f);
				plat3.transform.position = Vector3.Lerp (plat3.transform.position, pos3.transform.position, 0.04f);
				plat4.transform.position = Vector3.Lerp (plat4.transform.position, pos4.transform.position, 0.04f);
				plat5.transform.position = Vector3.Lerp (plat5.transform.position, pos5.transform.position, 0.035f);
				plat6.transform.position = Vector3.Lerp (plat6.transform.position, pos6.transform.position, 0.03f);
				plat7.transform.position = Vector3.Lerp (plat7.transform.position, pos7.transform.position, 0.04f);
				plat8.transform.position = Vector3.Lerp (plat8.transform.position, pos8.transform.position, 0.035f);
				plat9.transform.position = Vector3.Lerp (plat9.transform.position, pos9.transform.position, 0.029f);
				plat10.transform.position = Vector3.Lerp (plat10.transform.position, pos10.transform.position, 0.04f);
				plat11.transform.position = Vector3.Lerp (plat11.transform.position, pos11.transform.position, 0.04f);
				plat12.transform.position = Vector3.Lerp (plat12.transform.position, pos12.transform.position, 0.04f);
				plat13.transform.position = Vector3.Lerp (plat13.transform.position, pos13.transform.position, 0.04f);
			} else if (cristalScript.Phase2Start == true && phase2Up == false) {
				//descer para segunda fase
				print ("descer segunda fase");
				plat1.transform.position = Vector3.Lerp (plat1.transform.position, backPos1.transform.position, 0.03f);
				plat2.transform.position = Vector3.Lerp (plat2.transform.position, backPos2.transform.position, 0.035f);
				plat3.transform.position = Vector3.Lerp (plat3.transform.position, backPos3.transform.position, 0.04f);
				plat4.transform.position = Vector3.Lerp (plat4.transform.position, backPos4.transform.position, 0.04f);
				plat5.transform.position = Vector3.Lerp (plat5.transform.position, backPos5.transform.position, 0.035f);
				plat6.transform.position = Vector3.Lerp (plat6.transform.position, backPos6.transform.position, 0.03f);
				plat7.transform.position = Vector3.Lerp (plat7.transform.position, backPos7.transform.position, 0.04f);
				plat8.transform.position = Vector3.Lerp (plat8.transform.position, backPos8.transform.position, 0.035f);
				plat9.transform.position = Vector3.Lerp (plat9.transform.position, backPos9.transform.position, 0.029f);
				plat10.transform.position = Vector3.Lerp (plat10.transform.position, backPos10.transform.position, 0.04f);
				plat11.transform.position = Vector3.Lerp (plat11.transform.position,backPos11.transform.position, 0.04f);
				plat12.transform.position = Vector3.Lerp (plat12.transform.position, backPos12.transform.position, 0.04f);
				plat13.transform.position = Vector3.Lerp (plat13.transform.position, backPos13.transform.position, 0.04f);
				if (plat1.transform.position.y <= backPos6.transform.position.y) {
					//tempo ate subir para segunda fase
					print ("tempo para subir segunda fase");
					StartCoroutine ("ChangePhase2Pos", 0f);
				}
			} else if (phase2Up == true && cristalScript.Phase2Start == true && cristalScript.Phase3Start == false) {
				//subir segunda fase
				print ("subir segunda fase");
				plat21.transform.position = Vector3.Lerp (plat21.transform.position, phase2Pos1.transform.position, 0.03f);
				plat22.transform.position = Vector3.Lerp (plat22.transform.position, phase2Pos2.transform.position, 0.035f);
				plat23.transform.position = Vector3.Lerp (plat23.transform.position, phase2Pos3.transform.position, 0.04f);
				plat24.transform.position = Vector3.Lerp (plat24.transform.position, phase2Pos4.transform.position, 0.04f);
				plat25.transform.position = Vector3.Lerp (plat25.transform.position, phase2Pos5.transform.position, 0.035f);
				plat26.transform.position = Vector3.Lerp (plat26.transform.position, phase2Pos6.transform.position, 0.03f);
				plat27.transform.position = Vector3.Lerp (plat27.transform.position, phase2Pos7.transform.position, 0.04f);
				plat28.transform.position = Vector3.Lerp (plat28.transform.position, phase2Pos8.transform.position, 0.035f);
				plat29.transform.position = Vector3.Lerp (plat29.transform.position, phase2Pos9.transform.position, 0.029f);
				plat210.transform.position = Vector3.Lerp (plat210.transform.position, phase2Pos10.transform.position, 0.04f);
				plat211.transform.position = Vector3.Lerp (plat211.transform.position, phase2Pos11.transform.position, 0.04f);
				plat212.transform.position = Vector3.Lerp (plat212.transform.position, phase2Pos12.transform.position, 0.04f);
				defensePlat1.transform.position = Vector3.Lerp (defensePlat1.transform.position, dfPos1.transform.position, 0.04f);
				defensePlat2.transform.position = Vector3.Lerp (defensePlat2.transform.position, dfPos2.transform.position, 0.04f);
				defensePlat3.transform.position = Vector3.Lerp (defensePlat3.transform.position, dfPos3.transform.position, 0.04f);
				defensePlat4.transform.position = Vector3.Lerp (defensePlat4.transform.position, dfPos4.transform.position, 0.04f);
				defensePlat5.transform.position = Vector3.Lerp (defensePlat5.transform.position, dfPos5.transform.position, 0.04f);
				defensePlat6.transform.position = Vector3.Lerp (defensePlat6.transform.position, dfPos6.transform.position, 0.04f);
				cristalScript.Phase3CanStart = true;
			} else if (cristalScript.Phase3Start == true && phase3Up == false) {
				//descer para terceira fase
				print ("descer para terceira fase");
				plat21.transform.position = Vector3.Lerp (plat21.transform.position, back2Pos1.transform.position, 0.03f);
				plat22.transform.position = Vector3.Lerp (plat22.transform.position, back2Pos2.transform.position, 0.035f);
				plat23.transform.position = Vector3.Lerp (plat23.transform.position, back2Pos3.transform.position, 0.04f);
				plat24.transform.position = Vector3.Lerp (plat24.transform.position, back2Pos4.transform.position, 0.04f);
				plat25.transform.position = Vector3.Lerp (plat25.transform.position, back2Pos5.transform.position, 0.035f);
				plat26.transform.position = Vector3.Lerp (plat26.transform.position, back2Pos6.transform.position, 0.03f);
				plat27.transform.position = Vector3.Lerp (plat27.transform.position, back2Pos7.transform.position, 0.04f);
				plat28.transform.position = Vector3.Lerp (plat28.transform.position, back2Pos8.transform.position, 0.035f);
				plat29.transform.position = Vector3.Lerp (plat29.transform.position, back2Pos9.transform.position, 0.025f);
				plat210.transform.position = Vector3.Lerp (plat210.transform.position, back2Pos10.transform.position, 0.04f);
				plat211.transform.position = Vector3.Lerp (plat211.transform.position, back2Pos11.transform.position, 0.04f);
				plat212.transform.position = Vector3.Lerp (plat212.transform.position, back2Pos12.transform.position, 0.04f);
				defensePlat1.transform.position = Vector3.Lerp (defensePlat1.transform.position, back2Pos1.transform.position, 0.04f);
				defensePlat2.transform.position = Vector3.Lerp (defensePlat2.transform.position, back2Pos1.transform.position, 0.04f);
				defensePlat3.transform.position = Vector3.Lerp (defensePlat3.transform.position, back2Pos1.transform.position, 0.04f);
				defensePlat4.transform.position = Vector3.Lerp (defensePlat4.transform.position, back2Pos1.transform.position, 0.04f);
				defensePlat5.transform.position = Vector3.Lerp (defensePlat5.transform.position, back2Pos1.transform.position, 0.04f);
				defensePlat6.transform.position = Vector3.Lerp (defensePlat6.transform.position, back2Pos1.transform.position, 0.04f);
				if (plat7.transform.position.y <= back2Pos6.transform.position.y) {
					//tempo ate subir para segunda fase
					print ("tempo para subir terceira fase");
					StartCoroutine ("ChangePhase3Pos", 0f);
				}
			} else if (phase3Up == true && cristalScript.Phase3Start == true && cristalScript.End == false) {
				// subir terceira fase
				print("subir terceira fase");
				plat31.transform.position = Vector3.Lerp (plat31.transform.position, phase3Pos1.transform.position, 0.03f);
				plat32.transform.position = Vector3.Lerp (plat32.transform.position, phase3Pos2.transform.position, 0.035f);
				plat33.transform.position = Vector3.Lerp (plat33.transform.position, phase3Pos3.transform.position, 0.04f);
				plat34.transform.position = Vector3.Lerp (plat34.transform.position, phase3Pos4.transform.position, 0.04f);
				plat35.transform.position = Vector3.Lerp (plat35.transform.position, phase3Pos5.transform.position, 0.035f);
				plat36.transform.position = Vector3.Lerp (plat36.transform.position, phase3Pos6.transform.position, 0.029f);
				plat37.transform.position = Vector3.Lerp (plat37.transform.position, phase3Pos7.transform.position, 0.04f);
				plat38.transform.position = Vector3.Lerp (plat38.transform.position, phase3Pos8.transform.position, 0.035f);
				plat39.transform.position = Vector3.Lerp (plat39.transform.position, phase3Pos9.transform.position, 0.03f);
				plat310.transform.position = Vector3.Lerp (plat310.transform.position, phase3Pos10.transform.position, 0.04f);
				plat311.transform.position = Vector3.Lerp (plat311.transform.position, phase3Pos11.transform.position, 0.04f);
				plat312.transform.position = Vector3.Lerp (plat312.transform.position, phase3Pos12.transform.position, 0.04f);
				plat313.transform.position = Vector3.Lerp (plat313.transform.position, phase3Pos13.transform.position, 0.04f);
				cristalScript.CanKillBoss = true;
			} else if (cristalScript.End == true) {
				//descer para acabar
				print ("descer para acabar");
				plat31.transform.position = Vector3.Lerp (plat31.transform.position, back3Pos1.transform.position, 0.03f);
				plat32.transform.position = Vector3.Lerp (plat32.transform.position, back3Pos2.transform.position, 0.035f);
				plat33.transform.position = Vector3.Lerp (plat33.transform.position, back3Pos3.transform.position, 0.04f);
				plat34.transform.position = Vector3.Lerp (plat34.transform.position, back3Pos4.transform.position, 0.04f);
				plat35.transform.position = Vector3.Lerp (plat35.transform.position, back3Pos5.transform.position, 0.035f);
				plat36.transform.position = Vector3.Lerp (plat36.transform.position, back3Pos6.transform.position, 0.04f);
				plat37.transform.position = Vector3.Lerp (plat37.transform.position, back3Pos7.transform.position, 0.04f);
				plat38.transform.position = Vector3.Lerp (plat38.transform.position, back3Pos8.transform.position, 0.035f);
				plat39.transform.position = Vector3.Lerp (plat39.transform.position, back3Pos9.transform.position, 0.03f);
				plat310.transform.position = Vector3.Lerp (plat310.transform.position, back3Pos10.transform.position, 0.04f);
				plat311.transform.position = Vector3.Lerp (plat311.transform.position, back3Pos11.transform.position, 0.04f);
				plat312.transform.position = Vector3.Lerp (plat312.transform.position, back3Pos12.transform.position, 0.04f);
				plat313.transform.position = Vector3.Lerp (plat313.transform.position, back3Pos13.transform.position, 0.04f);
			}


		
			//////phase2

		}
		/*if (plat6.transform.position == pos6.transform.position) {
			platsMove = false;

		}
		if(cristalScript.Phase2Start == true){
			platsMove = true;
			disapearBool = true;
		}*/
	}



	IEnumerator BossCutscene(){
		fallRock.bodyType = RigidbodyType2D.Dynamic;
		yield return new WaitForSeconds (2.5f);
		platsMove = true;
		stopCouroutine = true;

	}

	IEnumerator ChangePhase2Pos(){
		yield return new WaitForSeconds (4f);
		phase2Up = true;
		//StopCoroutine ("ChangePhase2Pos");
	}

	IEnumerator ChangePhase3Pos(){
		yield return new WaitForSeconds (4f);
		phase3Up = true;
		//StopCoroutine ("ChangePhase2Pos");
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			startCutscene = true;
		}

	}
}
