﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningPlatformScript : MonoBehaviour {

	private GameManager gmScript;

	[SerializeField]
	private GameObject burn1Obj;
	[SerializeField]
	private GameObject burn2Obj;
	[SerializeField]
	private GameObject burn3Obj;

	private bool reTimer;
	private bool startBurn;

	private bool burnKill;

	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		burn1Obj.SetActive (false);
		burn2Obj.SetActive (false);
		burn3Obj.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		burn1Obj.transform.position = transform.position;
		burn2Obj.transform.position = transform.position;
		burn3Obj.transform.position = transform.position;
		if(startBurn == true){
			StartCoroutine ("BurnTimer",0f);
			reTimer = true;
		}

	}

	IEnumerator BurnTimer(){
		
		startBurn = false;
		burnKill = false;
		burn1Obj.SetActive (false);
		burn2Obj.SetActive (false);
		burn3Obj.SetActive (false);
		yield return new WaitForSeconds (0.5f);
		burn1Obj.SetActive (true);
		burn2Obj.SetActive (false);
		burn3Obj.SetActive (false);
		yield return new WaitForSeconds (1f);
		burn1Obj.SetActive (false);
		burn2Obj.SetActive (true);
		burn3Obj.SetActive (false);
		yield return new WaitForSeconds (1f);
		burn1Obj.SetActive (false);
		burn2Obj.SetActive (false);
		burn3Obj.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		burnKill = true;
		yield return new WaitForSeconds (1f);
		burn1Obj.SetActive (false);
		burn2Obj.SetActive (false);
		burn3Obj.SetActive (false);
		burnKill = false;
		reTimer = false;
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if(reTimer == false){
				startBurn = true;
			}

		}
	}
	void OnTriggerStay2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if(burnKill == true){
				gmScript.Death = true;
			}
		}
	}

}
