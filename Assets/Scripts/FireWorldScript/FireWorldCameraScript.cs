﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWorldCameraScript : MonoBehaviour {

	public Transform yin, yang;

	public float smoothTime = 0.3F;
	private Vector3 velocity = Vector3.zero;

	private bool ismap = false;

	private bool cameraIsFar;

	public bool CameraIsFar {
		get {
			return cameraIsFar;
		}
	}

	[SerializeField]
	private float heightCameralvl;

	public float HeightCameralvl {
		get {
			return heightCameralvl;
		}
		set {
			heightCameralvl = value;
		}
	}
	[SerializeField]
	private GameObject fireBackground;

	//[SerializeField]
	//private GameObject eggBackground;
	private Vector2 middle;
	private Vector2 currentPos;

	private float minSizeY;
	private float maxSizeY;

	[SerializeField]
	private Transform minSizeObj;
	[SerializeField]
	private Transform maxSizeObj;
	[SerializeField]
	private Transform minSizeCamObj;


	private bool goUp;
	private bool goDown;

	void Start(){
		heightCameralvl = 4.5f;
	}


	// Update is called once per frame
	void Update () {
		minSizeObj.position = new Vector3 (middle.x,minSizeY,-10f);
		maxSizeObj.position = new Vector3 (middle.x,maxSizeY,-10f);
		minSizeCamObj.position = new Vector3 (transform.position.x,transform.position.y-7f,-10f);
		SetCameraPos ();
		fireBackground.transform.position = new Vector2 (transform.position.x,transform.position.y);
		//eggBackground.transform.position = new Vector2 (transform.position.x,transform.position.y);
		//eggBackground.transform.position = Vector3.SmoothDamp (new Vector3(eggBackground.transform.position.x,eggBackground.transform.position.x,eggBackground.transform.position.z), new Vector3(transform.position.x,transform.position.y,transform.position.z),ref velocity.x,3f);
		/*if (ismap == false) {
			SetCameraPos ();

		}

		ShowMap ();
		*/
	}

	void SetCameraPos()
	{

		middle = (yin.position + yang.position) / 2f;
		minSizeY = middle.y-5;
		maxSizeY = middle.y+5;

		//transform.position = new Vector3(middle.x,middle.y+5,-15f);


		//currentPos.y = Mathf.Clamp (currentPos.y, minSizeY, maxSizeY);
		//transform.position = new Vector3(currentPos.x,currentPos.y,-15f);

		//GetComponent<Camera> ().transform.position = new Vector3 (middle.x, middle.y +heightCameralvl, GetComponent<Camera> ().transform.position.z);
		//transform.position = new Vector3 (middle.x, Mathf.Clamp(transform.position.y,middle.y,middle.y+3f), transform.position.z);


		/*if (middle.y >= GetComponent<Camera> ().transform.position.y) {
			transform.position = new Vector3 (middle.x, middle.y, transform.position.z);
		} else {
			//transform.position = new Vector3 (middle.x, transform.position.y, transform.position.z);
			//transform.position = new Vector3 (middle.x, Mathf.Clamp(transform.position.y,middle.y+4,middle.y+10), transform.position.z);


		}*/
		/*if(goUp == true){
		if (transform.position.y <= maxSizeY) {
			transform.position = new Vector3 (middle.x, transform.position.y + 0.06f, -15f);

		} else
			goUp = false;

	}else GetComponent<Camera> ().transform.position = new Vector3 (middle.x, Mathf.Clamp (transform.position.y, minSizeY, maxSizeY), GetComponent<Camera> ().transform.position.z);
	*/
	GetComponent<Camera> ().transform.position = new Vector3 (middle.x, Mathf.Clamp (transform.position.y, minSizeY, maxSizeY), GetComponent<Camera> ().transform.position.z);
	if (maxSizeObj.position.y >= transform.position.y+7) {
		transform.position = new Vector3 (middle.x, Mathf.Lerp(transform.position.y,transform.position.y + 5f,Time.deltaTime*2), -15f);

	}
	if(minSizeObj.position.y >= transform.position.y-6){
		transform.position = new Vector3 (middle.x, Mathf.Lerp(transform.position.y,transform.position.y + 5f,Time.deltaTime), -15f);
		//transform.position = new Vector3 (middle.x, transform.position.y + 0200f, -15f);
	}


	if (yin.position.y <= yang.position.y - 7.1f || yang.position.y <= yin.position.y - 7.1f) {
		GetComponent<Camera> ().orthographicSize = Mathf.SmoothDamp (GetComponent<Camera> ().orthographicSize, 14f, ref velocity.x, smoothTime);
		cameraIsFar = true;
	} else {
		GetComponent<Camera>().orthographicSize = Mathf.SmoothDamp (GetComponent<Camera>().orthographicSize, 10f, ref velocity.x, smoothTime);
		cameraIsFar = false;
	}
}


	void FixedUpdate(){


	}

	/*void ShowMap(){
	

		if (Input.GetKey(KeyCode.M)) {
			ismap = true;
			Debug.Log ("hello");
			Vector3 targetPosition = target.position;
			transform.position = Vector3.SmoothDamp (transform.position, targetPosition, ref velocity, smoothTime);
			GetComponent<Camera>().orthographicSize = Mathf.SmoothDamp (GetComponent<Camera>().orthographicSize, 100, ref velocity.x, smoothTime);
		}

		else if(Input.GetKeyUp (KeyCode.M)){
				ismap = false;

		}
	
	}*/
}
