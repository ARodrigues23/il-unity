﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CristalBossManagerScript : MonoBehaviour {

	private bool phase2Start;

	public bool Phase2Start {
		get {
			return phase2Start;
		}
	}

	private bool phase3Start;

	public bool Phase3Start {
		get {
			return phase3Start;
		}
	}

	private bool phase3CanStart;

	public bool Phase3CanStart {
		get {
			return phase3CanStart;
		}
		set {
			phase3CanStart = value;
		}
	}

	private bool canKillBoss;

	public bool CanKillBoss {
		get {
			return canKillBoss;
		}
		set {
			canKillBoss = value;
		}
	}


	private bool end;

	public bool End {
		get {
			return end;
		}
	}

	private float destroyTime;
	private bool startTime;
	private bool stopTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (startTime == true && stopTime == false) {
			print ("started");
			destroyTime += Time.deltaTime * 1;
		} 

		if(destroyTime >= 2.5f){
			stopTime = true;
			destroyTime = 0;
		}
	}

	void OnTriggerStay2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if (Input.GetKey (KeyCode.S) && Input.GetKey (KeyCode.DownArrow)) {
				
				startTime = true;
				stopTime = false;
				if(phase2Start == true){
					stopTime = false;
				}
				if(phase3Start == true){
					stopTime = false;
				}
				if(end == true){
					stopTime = false;
				}


				if (phase2Start == false && destroyTime >= 2) {
					
					phase2Start = true;


		
				} else if (phase3Start == false && phase3CanStart == true && canKillBoss == false && destroyTime >= 2) {
					phase3Start = true;


				
				} else if (phase3Start == true && canKillBoss == true && destroyTime >= 2) {
					end = true;



				}
			}
			if (Input.GetKeyUp (KeyCode.S) && Input.GetKeyUp (KeyCode.DownArrow)) {
				destroyTime = 0;
				stopTime = true;
			}

		} 
	}


}
