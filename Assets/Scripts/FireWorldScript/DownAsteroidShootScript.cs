﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownAsteroidShootScript : MonoBehaviour {

	private GameManager gmScript;

	private Vector3 curPos;
	private Vector3 lastPos;

	[SerializeField]
	private float speed;

	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		Destroy (this.gameObject, 7f);
	}

	// Update is called once per frame
	void Update () {

		//rb.AddForce (yangPos.transform.position * 3f);
		//rb.AddForceAtPosition (Vector2.down*3, yinPos.transform.position);
		//transform.position = Vector3.Lerp (transform.position, goToPos, 6f * Time.deltaTime);
		transform.position = Vector3.MoveTowards (transform.position, new Vector3(transform.position.x,transform.position.y-30f,transform.position.z), speed * Time.deltaTime);
		CheckStopMovement ();
	}

	void CheckStopMovement(){
		curPos = transform.position;
		if(curPos == lastPos){
			Destroy (this.gameObject);
		}
		lastPos = curPos;
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			gmScript.Death = true;
			Destroy (this.gameObject);

		}
	}
}
