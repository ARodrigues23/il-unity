﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineScript : MonoBehaviour {

	[SerializeField]
	private LineRenderer lineManager;
	[SerializeField]
	private Transform yinPos;
	[SerializeField]
	private Transform yangPos;
	[SerializeField]
	private SpringJoint2D springJointManager;
	[SerializeField]
	private DistanceJoint2D distanceJointManager;

	private Rigidbody2D yinRb2D;
	private Rigidbody2D yangRb2D;

	private PlayerControllerYin yinScript;
	private PlayerControllerYang yangScript;

	private float distance;
	[SerializeField]
	private Vector3 yangPosVect;

	[SerializeField]
	private float maxDistance;
	[SerializeField]
	private float minDistance;
	[SerializeField]
	private float standartDistance;
	[SerializeField]
	private float actualDistance;


	// Use this for initialization
	void Start () {
		springJointManager = GameObject.Find ("YinPlayer").GetComponent<SpringJoint2D> ();
		lineManager = GameObject.Find ("YinPlayer").GetComponent<LineRenderer> ();
		yinRb2D = GameObject.Find ("YinPlayer").GetComponent<Rigidbody2D> ();
		yangRb2D = GameObject.Find ("YangPlayer").GetComponent<Rigidbody2D> ();
		yinScript = FindObjectOfType<PlayerControllerYin> ();
		yangScript = FindObjectOfType<PlayerControllerYang> ();
	}
	
	// Update is called once per frame
	void Update () {
		distance = Vector2.Distance (transform.position, yangPosVect);
		yangPosVect = GameObject.Find ("YangPlayer").transform.position;
		SpringJointAdjusts ();
		LineElementPos ();
		Activejoints ();
		AddForceBoost ();
	}

	void LineElementPos(){
		lineManager.SetPosition (0, yinPos.transform.position);
		lineManager.SetPosition (1, yangPos.transform.position);
	}
	void SpringJointAdjusts(){

		actualDistance = distance;

		if(distance <= maxDistance){
			springJointManager.distance = actualDistance ;
			springJointManager.frequency = actualDistance / 4;
		}

		/*if(distance >= maxDistance){
			print ("far");
		}
		else if(distance <= minDistance && cordaManager.distance >= 1){
			cordaManager.distance -= Time.deltaTime;
		}*/
	}
	void Activejoints(){
		if(distance >= maxDistance){
			springJointManager.enabled = true;
		}
		if(distance <= maxDistance){
			springJointManager.enabled = false;
		}
	}

	void AddForceBoost(){
		if(yangScript.Stick == true){
			if(yinPos.transform.position.y > yangPos.transform.position.y){

				if(yinPos.transform.position.x < yangPos.transform.position.x){
					if(yinScript.Grounded == false && yangScript.Grounded == true){
						//yinRb2D.velocity = new Vector2 (yinRb2D.velocity.x, 0);
						yinRb2D.AddForce (new Vector2 (yinRb2D.velocity.x, 20));
						//print("top left yin");
					}

				}
				if(yinPos.transform.position.x > yangPos.transform.position.x){
					if(yinScript.Grounded == false && yangScript.Grounded == true){
						//yinRb2D.velocity = new Vector2 (yinRb2D.velocity.x, 0);
						yinRb2D.AddForce (new Vector2 (yinRb2D.velocity.x, 20));
						//print("top right yin");
					}
				}


			}
			if(yinPos.transform.position.y < yangPos.transform.position.y){
				if(yinPos.transform.position.x < yangPos.transform.position.x){
					//print ("bottom left yin");
				}
				if(yinPos.transform.position.x > yangPos.transform.position.x){
					//print ("bottom rigth yin");
				}


			}
		}
		if(yinScript.Stick == true){
			if(yangPos.transform.position.y > yinPos.transform.position.y){
				if(yangPos.transform.position.x < yinPos.transform.position.x){
					if(yangScript.Grounded == false && yinScript.Grounded == true){
						//yinRb2D.velocity = new Vector2 (yinRb2D.velocity.x, 0);
						yangRb2D.AddForce (new Vector2 (yangRb2D.velocity.x, 20));

						//print("top left yang");
					}
				}
				if(yangPos.transform.position.x > yinPos.transform.position.x){
					if(yangScript.Grounded == false && yinScript.Grounded == true){
						//yinRb2D.velocity = new Vector2 (yinRb2D.velocity.x, 0);
						yangRb2D.AddForce (new Vector2 (yangRb2D.velocity.x, 20));
						//print("top right yang");
					}
				}


			}
			if(yangPos.transform.position.y < yinPos.transform.position.y){
				if(yangPos.transform.position.x < yinPos.transform.position.x){
					//print ("bottom left yang");
				}
				if(yangPos.transform.position.x > yinPos.transform.position.x){
					//print ("bottom rigth yang");
				}


			}
		}


	}
}
