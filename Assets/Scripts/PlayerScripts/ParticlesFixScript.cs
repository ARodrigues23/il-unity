﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]
public class ParticlesFixScript : MonoBehaviour {
	ParticleSystem ps;
	ParticleSystem.Particle[] m_Particles;
	public Transform target;
	public float speed;
	int numParticlesAlive;
	void Start () {
		ps = GetComponent<ParticleSystem>();
		if (!GetComponent<Transform>()){
			GetComponent<Transform>();
		}
	}
	void Update () {

		Vector3 source = transform.position;
		Vector3 dest = target.position;
		float dx = dest.x - source.x;
		float dy = dest.y - source.y;

		m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
		numParticlesAlive = ps.GetParticles(m_Particles);
		float stepX = dx / numParticlesAlive;
		float stepY = dy / numParticlesAlive;

		float step = speed * Time.deltaTime;
		for (int i = 0; i < numParticlesAlive; i++) {
			//m_Particles[i].position = Vector3.Lerp(m_Particles[i].position, target.position, step);
			m_Particles[i].position = new Vector3(source.x + i * stepX, source.y + i * stepY, 0);
		}
		ps.SetParticles(m_Particles, numParticlesAlive);
	}
}
