﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerYang : MonoBehaviour {

	[SerializeField]
	private float speed;
	[SerializeField]
	private float maxspeed;

	private bool grounded;

	public bool Grounded {
		get {
			return grounded;
		}
		set {
			grounded = value;
		}
	}

	private float stickTime;
	private bool stick;

	public bool Stick {
		get {
			return stick;
		}
	}

	[SerializeField]
	private float jump;
	[SerializeField]
	private int jumpCount;
	[SerializeField]
	private int jumpCountAir;
	[SerializeField]
	private int jumpCountAirStay;


	[SerializeField]
	private float jumpCooldown;
	private float jumpCooldownTimer;
	private bool doubleJumped;
	private bool canSecondJump;
	private bool canAirSecondJump;
	private bool stayAirSecondJump;
	private bool landed;

	public bool Landed {
		get {
			return landed;
		}
		set {
			landed = value;
		}
	}


	private Rigidbody2D rb2D;
	private Animator anim;


	private GameManager gmScript;

	private float gravityScaleValue;

	public float GravityScaleValue {
		get {
			return gravityScaleValue;
		}
		set {
			gravityScaleValue = value;
		}
	}

	void Start () 
	{
		
		rb2D = gameObject.GetComponent<Rigidbody2D>();
		anim = gameObject.GetComponent<Animator> ();

		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		gravityScaleValue = 3;
	}

	void Update()
	{
		if(gmScript.Death== false && gmScript.DontMove == false){
			anim.SetFloat ("Walk", Mathf.Abs (Input.GetAxis ("YangHorizontal")));

			if (Input.GetAxis ("YangHorizontal") < -0.1f) 
			{
				transform.localScale = new Vector3 (-0.7f, 0.7f, 1);
			}

			if (Input.GetAxis ("YangHorizontal") > 0.1f) 
			{
				transform.localScale = new Vector3 (0.7f,0.7f, 1);
			}

			Jump ();
		}
	
	


		if(grounded == false ){
			jumpCooldownTimer += Time.deltaTime;
		}else if(grounded == true){
			jumpCooldownTimer = 0;
		}


		if (Input.GetKey (KeyCode.DownArrow) && grounded == true) {
			stick = true;
			anim.SetBool ("Release", false);
			anim.SetBool ("Stick", true);
			stickTime += Time.deltaTime;

			if (stickTime >= 0.4f) {
				anim.SetBool ("Release", false);
				anim.SetBool ("Stick", false);
				anim.SetBool ("StickHold", true);

			}
			rb2D.mass = 500;

		} else
			rb2D.mass = 1;

		if(!Input.GetKey (KeyCode.DownArrow)){
			stick = false;
			stickTime = 0;
			anim.SetBool ("Release", true);
			anim.SetBool ("Stick", false);
			anim.SetBool ("StickHold",false);
		}

		rb2D.gravityScale = gravityScaleValue;

		if(gmScript.Death == true){
			rb2D.velocity = new Vector2 (0, 0);
			anim.SetBool ("Jump",false);
			anim.SetBool ("DoubleJump",false);
			anim.SetBool ("Release", false);
			anim.SetBool ("Stick", false);
			anim.SetBool ("StickHold",false);
			anim.SetBool ("Death",true);
		}else if(gmScript.Death == false){
			anim.SetBool ("Death",false);
		}

	}

	void FixedUpdate ()
	{
		if(gmScript.Death== false && gmScript.DontMove == false){
			float a = Input.GetAxis ("YangHorizontal");

			rb2D.AddForce ((Vector2.right * speed) * a);

			if (rb2D.velocity.x > maxspeed) {
				rb2D.velocity = new Vector2 (maxspeed, rb2D.velocity.y);
			}

			if (rb2D.velocity.x < -maxspeed) {
				rb2D.velocity = new Vector2 (-maxspeed, rb2D.velocity.y);
			}
			
		}

	

	}

	void Jump()
	{

		if (Input.GetKeyDown (KeyCode.UpArrow) && stick == false) {
			if (grounded == true) {
				if(jumpCount == 0){
					anim.SetBool ("Jump",true);
					anim.SetBool ("DoubleJump",false);
					rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
					rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
					canSecondJump = true;
					jumpCount++;
				}

			} else {
				if (canSecondJump == true) {
					if(Input.GetKeyDown(KeyCode.UpArrow)){
						if(jumpCount == 1){
							anim.SetBool ("Jump",false);
							anim.SetBool ("DoubleJump",true);
							canSecondJump = false;
							doubleJumped = true;
							rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
							rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
						}

					}
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			if (grounded == false) {
				if(jumpCountAir == 0){
					if(doubleJumped == false){
						anim.SetBool ("Jump",true);
						anim.SetBool ("DoubleJump",false);
						rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
						rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
						canAirSecondJump = true;
						jumpCooldownTimer = 0;
						jumpCountAir++;
					}

				}else {
					if (canAirSecondJump == true) {
						if(Input.GetKeyDown (KeyCode.UpArrow)){
							anim.SetBool ("Jump",false);
							anim.SetBool ("DoubleJump",true);
							canAirSecondJump = false;
							rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
							rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
						}
					}
				}  
			}
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			if (grounded == false) {
				if(jumpCountAirStay == 0){
					if(jumpCooldownTimer >= 2f){
						anim.SetBool ("Jump",true);
						anim.SetBool ("DoubleJump",false);
						rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
						rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
						stayAirSecondJump = true;
						jumpCooldownTimer = 0;
						jumpCountAirStay++;
					}


				}else {
					if (stayAirSecondJump == true) {
						if(Input.GetKeyDown (KeyCode.UpArrow)){
							if(jumpCooldownTimer >= 1f){
								anim.SetBool ("Jump",false);
								anim.SetBool ("DoubleJump",true);
								stayAirSecondJump = false;
								rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
								rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
								jumpCooldownTimer = 0;
								jumpCountAirStay = 0;
							}

						}
					}
				}  
			}
		}
		/*if (grounded == false && jumped == true ) {
			StartCoroutine ("LandTiming", 0f);
		}
		*/
		//if(rb2D.velocity.y < -14f && landed == true){
		//	StartCoroutine ("LandTiming", 0f);

		//}
		if(landed == true){
			StartCoroutine ("LandTiming", 0f);

		}



		/*if (jumpCooldownTimer <= 0.01f) 
		{
			canSecondJump = false;
			doubleJumped = false;
			jumpCooldownTimer = 0;
			jumpCount = 0;
		}
		if (jumpCount == 2) {
			doubleJumped = true;

		}

		if (Input.GetKeyDown(KeyCode.W) && jumpCooldownTimer == 0 && jumpCount == 0 && canSecondJump == false) 
		{
			anim.SetBool ("Landing", false);
			anim.SetBool("Jump",true);
			rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
			rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
			jumpCooldownTimer = jumpCooldown;
			canSecondJump = true;
			jumpCount = 1;
			print ("ficou a um");

		} 
		else if (Input.GetKeyDown(KeyCode.W) && jumpCooldownTimer >= 0.01f && canSecondJump == true && jumpCount == 1 && grounded == false) 
		{
			anim.SetBool ("Jump",false);
			anim.SetBool ("DoubleJump",true);
			rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
			rb2D.AddForce (new Vector2 (rb2D.velocity.x, jump));
			jumpCount = 2;
			jumpCooldownTimer = jumpCooldown;
		} 
		if(grounded == true && jumpCooldownTimer >= 0.01f){
			anim.SetBool ("Jump",false);
			anim.SetBool ("DoubleJump",false);
			anim.SetBool ("Landing", true);
		}
		if (grounded == true) {
			jumpCooldownTimer = 0;

		}*/


	}

	IEnumerator LandTiming(){
		anim.SetBool ("Jump",false);
		anim.SetBool ("DoubleJump", false);
		anim.SetBool ("Landing", true);
		yield return new WaitForSeconds (0.4f);
		anim.SetBool ("Landing", false);

	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.tag == "Death"){
			gmScript.Death = true;
		}
		if(other.transform.tag == "GravGoBack"){
			//gravityScaleValue = 3f;
		}

	}
	void OnTriggerStay2D(Collider2D other){
		if (other.transform.tag == "Water") {
			//gravityScaleValue = 1.5f;
		} 
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.transform.tag == "MovingPlatform") 
		{
			transform.parent = other.transform;
			doubleJumped = false;
			grounded = true;
			jumpCount = 0;
			jumpCountAir = 0;
			jumpCountAirStay = 0;
		}
		if (other.transform.tag == "Floor") 
		{
			doubleJumped = false;
			grounded = true;
			jumpCount = 0;
			jumpCountAir = 0;
			jumpCountAirStay = 0;
		}
	}
	void OnCollisionStay2D(Collision2D other)
	{
		if (other.transform.tag == "MovingPlatform") 
		{
			transform.parent = other.transform;
			doubleJumped = false;
			grounded = true;
		}
		if (other.transform.tag == "Floor") 
		{
			doubleJumped = false;
			grounded = true;
		}
	}
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.transform.tag == "MovingPlatform") 
		{
			transform.parent = null;
			grounded = false;
		}
		if (other.transform.tag == "Floor") 
		{
			grounded = false;

		}
	}
}
