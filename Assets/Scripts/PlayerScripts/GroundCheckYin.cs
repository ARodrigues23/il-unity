﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckYin : MonoBehaviour {

	private PlayerControllerYin player;

	void Start () 
	{
		player = gameObject.GetComponentInParent<PlayerControllerYin> ();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.transform.tag == "Floor") 
		{
			player.Landed = true;

		}
		if (col.transform.tag == "MovingPlatform") 
		{
			player.Landed = true;

		}

	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.transform.tag == "Floor") 
		{
			player.Landed = false;


		}
		if (col.transform.tag == "MovingPlatform") 
		{
			player.Landed = false;
		
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.transform.tag == "Floor") 
		{
			player.Landed = false;


		}
		if (col.transform.tag == "MovingPlatform") 
		{
			player.Landed = false;

		}

	}
}
