﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Comecar(){
		SceneManager.LoadScene ("WoodWorld",LoadSceneMode.Single);
	}

	public void Settings(){
		SceneManager.LoadScene ("Settings",LoadSceneMode.Single);
	}

	public void Quit(){
		Application.Quit();
	}
}
