﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToFireWorldScript : MonoBehaviour {

	private bool load;
	private bool fadeOut;
	private bool fadeIn;
	[SerializeField]
	private Image blackScreen;

	AsyncOperation ao;
	public GameObject loadingScreenBG;
	public Slider progBar;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(load == true){
			fadeIn = false;
			fadeOut = true;
		}
	}

	void FixedUpdate(){
		FadeIn ();
		FadeOut ();
	}

	void FadeIn(){
		if(fadeIn == true){
			blackScreen.color = new Color (0f, 0f, 0f, blackScreen.color.a - 0.02f);

			if(blackScreen.color.a <= 0f){
				blackScreen.color = new Color (0f,0f,0f,0f);
			}
		}

	}
	void FadeOut(){
		if(fadeOut == true){
			blackScreen.color = new Color (0f, 0f, 0f, blackScreen.color.a + 0.02f);

			if(blackScreen.color.a >= 1f){
				blackScreen.color = new Color (0f,0f,0f,1f);
			}
		}

	}

	void LoadLevel01(){
		loadingScreenBG.SetActive (true);
		progBar.gameObject.SetActive (true);
		StartCoroutine ("LoadLevelWithRealProgress");
	}

	IEnumerator LoadLevelWithRealProgress()
	{
		yield return new WaitForSeconds (1);
		ao = SceneManager.LoadSceneAsync (5);
		ao.allowSceneActivation = false;

		while(!ao.isDone)
		{
			progBar.value = ao.progress;

			if(ao.progress == 0.9f)
			{
				progBar.value = 1f;
				yield return new WaitForSeconds (2);
				ao.allowSceneActivation = true;
			}
			yield return null;
		}
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer") {
			load = true;
			LoadLevel01 ();
		}
	}
}
