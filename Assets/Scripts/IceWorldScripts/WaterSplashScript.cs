﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSplashScript : MonoBehaviour {

	[SerializeField]
	private GameObject splash;

	[SerializeField]
	private Transform yinPos;
	[SerializeField]
	private Transform yangPos;

	private Vector3 curYinPos;
	private Vector3 curYangPos;
	private Vector3 lastYinPos;
	private Vector3 lastYangPos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer") {
			curYinPos = yinPos.transform.position;
			Instantiate (splash, curYinPos, Quaternion.identity);

		}
		if (other.transform.name == "YangPlayer") {
			curYangPos = yangPos.transform.position;
			Instantiate (splash, curYangPos, Quaternion.identity);

		}
	}
}
