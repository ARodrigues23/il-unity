﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComeBackDropingPlatScript : MonoBehaviour {

	private bool fall;

	private Vector2 startPos;

	private bool comeback;

	private bool fadeOut;
	private bool fadeIn;

	private Rigidbody2D rb2D;

	private SpriteRenderer sprite;

	[SerializeField]
	private float fallSeconds;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
		startPos = new Vector2 (transform.position.x,transform.position.y);
	}

	// Update is called once per frame
	void Update () {
		if(fall == true){
			StartCoroutine ("FallTime",fallSeconds);
		}
		if(comeback == true){
			StartCoroutine ("MoveBack",0.5f);
		}
		FadeIn ();
		FadeOut ();
	}

	void FixedUpdate(){
		//RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.up, 30);
		//Debug.DrawRay (transform.position,new Vector3(transform.position.x,transform.position.y,transform.position.z),Color.red);
		/*RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.up);

		if(hit.collider.gameObject.tag == ("Yin") || hit.collider.tag == ("Yang") ){
			print ("yas");
			StartCoroutine ("FallTime",fallSeconds);
		}*/
	}


	void FadeIn(){
		if(fadeIn == true){
			sprite.color = new Color (1f, 1f, 1f, sprite.color.a - 0.05f);

			if(sprite.color.a <= 0f){
				sprite.color = new Color (1f,1f,1f,0f);
			}
		}

	}
	void FadeOut(){
		if(fadeOut == true){
			sprite.color = new Color (1f, 1f, 1f, sprite.color.a + 0.03f);

			if(sprite.color.a >= 1f){
				sprite.color = new Color (1f,1f,1f,1f);
			}
		}

	}

	IEnumerator FallTime(float fallTime){
		print ("fall");
		StopCoroutine ("MoveBack");
		yield return new WaitForSeconds (fallTime);
		rb2D.bodyType = RigidbodyType2D.Dynamic;
	}

	IEnumerator MoveBack(float time){
		yield return new WaitForSeconds (time);
		transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
		fadeIn = false;
		fadeOut = true;
		rb2D.bodyType = RigidbodyType2D.Static;
		fall = false;
		transform.position = startPos;

	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			
			fall = true;
		}
	}

	void OnCollisionEnter2D (Collision2D other){

		if(other.transform.tag == "Floor"){
			fall = false;
			fadeIn = true;
			fadeOut = false;
			comeback = true;
		}

	}
}
