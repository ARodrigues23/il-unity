﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteWaterSplashScript : MonoBehaviour {

	[SerializeField]
	private float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Destroy (gameObject, time);
	}
}
