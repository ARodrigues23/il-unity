﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropingPlatformScript : MonoBehaviour {

	private bool fall;

	private Rigidbody2D rb2D;

	[SerializeField]
	private float fallSeconds;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(fall == true){
			StartCoroutine ("FallTime",fallSeconds);
		}
	}

	void FixedUpdate(){
		//RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.up, 30);
		//Debug.DrawRay (transform.position,new Vector3(transform.position.x,transform.position.y,transform.position.z),Color.red);
		/*RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.up);

		if(hit.collider.gameObject.tag == ("Yin") || hit.collider.tag == ("Yang") ){
			print ("yas");
			StartCoroutine ("FallTime",fallSeconds);
		}*/
	}

	IEnumerator FallTime(float fallTime){
		yield return new WaitForSeconds (fallTime);
		rb2D.bodyType = RigidbodyType2D.Dynamic;
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {

			fall = true;
		}
	}

	void OnCollisionEnter2D (Collision2D other){
		
		if(other.transform.tag == "Floor"){
			Destroy (gameObject,0.5f);
		}

	}
}
