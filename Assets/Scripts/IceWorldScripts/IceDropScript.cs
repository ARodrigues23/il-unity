﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceDropScript : MonoBehaviour {

	private GameManager gmScript;

	private bool fall;

	[SerializeField]
	private float fallTime;

	private Vector2 startPos;

	private bool comeback;

	private bool fadeOut;
	private bool fadeIn;

	private Rigidbody2D rb2D;

	private SpriteRenderer sprite;

	[SerializeField]
	private BoxCollider2D killColl;
	private bool canKill;

	private bool falling;

	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		rb2D = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
		killColl = GetComponent<BoxCollider2D> ();
		startPos = new Vector2 (transform.position.x,transform.position.y);
		canKill = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(fall == true){
			StartCoroutine ("FallTime",fallTime);
		}
		if(comeback == true){
			StartCoroutine ("MoveBack",1f);
		}
		FadeIn ();
		FadeOut ();
	}

	void FadeIn(){
		if(fadeIn == true){
			sprite.color = new Color (1f, 1f, 1f, sprite.color.a - 0.05f);

			if(sprite.color.a <= 0f){
				sprite.color = new Color (1f,1f,1f,0f);
			}
		}

	}
	void FadeOut(){
		if(fadeOut == true){
			sprite.color = new Color (1f, 1f, 1f, sprite.color.a + 0.03f);

			if(sprite.color.a >= 1f){
				sprite.color = new Color (1f,1f,1f,1f);
			}
		}

	}

	IEnumerator FallTime(float fallTime){
		StopCoroutine ("MoveBack");
		yield return new WaitForSeconds (fallTime);
		rb2D.bodyType = RigidbodyType2D.Dynamic;
	}

	IEnumerator MoveBack(float time){
		yield return new WaitForSeconds (time);
		falling = false;
		killColl.isTrigger = false;
		transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
		fadeIn = false;
		fadeOut = true;
		rb2D.bodyType = RigidbodyType2D.Static;
		fall = false;
		transform.position = startPos;
		canKill = true;
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if(falling == false){
				fall = true;
				falling = true;
			}

		}
	
	}

	void OnCollisionEnter2D (Collision2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {
			if(canKill == true){
				gmScript.Death = true;

			}

		}
		if(other.transform.tag == "Floor"){
			killColl.isTrigger = true;
			canKill = false;
			fall = false;
			fadeIn = true;
			fadeOut = false;
			comeback = true;
		}
		if(other.transform.tag == "MovingPlatform"){
			killColl.isTrigger = true;
			canKill = false;
			fall = false;
			fadeIn = true;
			fadeOut = false;
			comeback = true;
		}

	}
}
