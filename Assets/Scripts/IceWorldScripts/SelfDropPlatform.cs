﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDropPlatform : MonoBehaviour {



	private bool fall;

	private Rigidbody2D rb2D;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(fall == true){
			rb2D.bodyType = RigidbodyType2D.Dynamic;
			fall = false;
		}
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {

			fall = true;

		}

		if(other.transform.tag == "StopPlat"){
			
			rb2D.bodyType = RigidbodyType2D.Static;
		}
	}
	void OnCollisionEnter2D (Collision2D other){


	}
}
