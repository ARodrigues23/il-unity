﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkerManager : MonoBehaviour {

	[SerializeField]
	private float moveSpeed;
	private bool moveLeft;





	private GameManager gmScript;






	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	// Update is called once per frame
	void Update () {
		EnemyPatrol ();
	}

	void EnemyPatrol(){
		if (moveLeft) {
			transform.localScale = new Vector3 (1.5f, 1.5f, 1f);
			GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
		} else 
		{
			transform.localScale = new Vector3 (-1.5f, 1.5f, 1f);
			GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);	
		}
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.transform.tag == "EnemyGoBack") 
		{
			moveLeft = !moveLeft;
		}

	}
	void OnCollisionEnter2D (Collision2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {

			gmScript.Death = true;
		}

	}

}
