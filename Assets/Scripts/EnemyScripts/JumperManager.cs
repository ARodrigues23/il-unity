﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperManager : MonoBehaviour {

	[SerializeField]
	private float moveSpeed;
	private bool moveLeft;

	private Transform playerPos;

	private GameManager gmScript;

	private Vector3 player;
	private Vector3 playerMid;
	private Vector3 playerDirection;

	private float xDif;
	private float yDif;

	private Rigidbody2D rb2D;
	private Animator animator;

	private bool idle;

	private float distance;
	[SerializeField]
	private float givenDistance;

	[SerializeField]
	private float minHeightDistance;

	[SerializeField]
	private float maxHeightDistance;

	[SerializeField]
	private float minLengthDistance;


	[SerializeField]
	private float maxLengthDistance;

	private bool onGround;
	private bool jumped;

	private bool stun;
	private float stunTime;

	private Vector3 spawnPos;

	// Use this for initialization
	void Start () {
		gmScript = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		rb2D = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		stunTime = 0;
		stun = false;
		spawnPos = transform.position;
	}

	// Update is called once per frame
	void Update () {
		Ai ();
		StartCoroutine ("RespawnJumper",0f);
	}

	void FixedUpdate(){
		
	}

	void Ai(){
		distance = Vector2.Distance (player,transform.position);
		player = GameObject.Find ("YinPlayer").transform.position;

		StartCoroutine ("IdleSwitch",0f);

		if (stunTime > 0) {
			stunTime -= Time.deltaTime;
		} else
			stun = false;
		if(distance < givenDistance && !stun){
			xDif = player.x - transform.position.x;
			yDif = player.y - transform.position.y;

			playerDirection = new Vector2 (xDif,yDif);
			if (player.x < transform.position.x) {
				//transform.localScale = new Vector2 (1.25f, transform.localScale.y);
				if (jumped == false) {
					jumped = true;
					animator.SetBool ("Idle", false);
					animator.SetBool ("IdleEye", false);
					animator.SetBool ("TakeOff", true);

					rb2D.AddForce (new Vector2 (Random.Range(-minLengthDistance, -maxLengthDistance),(Random.Range(minHeightDistance,maxHeightDistance))));
					StartCoroutine ("JumpingTimer", 0f);
				} 


			}
			if (player.x > transform.position.x) {
				//transform.localScale = new Vector2 (-1.25f, transform.localScale.y);
				if (jumped == false) {
					jumped = true;
					animator.SetBool ("Idle", false);
					animator.SetBool ("IdleEye", false);
					animator.SetBool ("TakeOff", true);

					rb2D.AddForce (new Vector2 (Random.Range(minLengthDistance, maxLengthDistance),(Random.Range(minHeightDistance,maxHeightDistance))));
					StartCoroutine ("JumpingTimer", 0f);

				} 
			}


			//rb2D.velocity = new Vector2 (rb2D.velocity.x, 0);
			//rb2D.AddForce (new Vector2 (rb2D.velocity.x, 30));
		}



	}

	IEnumerator RespawnJumper(){
		if(gmScript.Death == true){
			yield return new WaitForSeconds (1f);
			animator.SetBool ("IdleEye", false);
			animator.SetBool ("TakeOff", false);
			animator.SetBool ("Landing", false);
			animator.SetBool ("Idle", true);
			transform.position = spawnPos;
		}
	}

	IEnumerator IdleSwitch(){
		if (idle == true) {
			animator.SetBool ("Idle", true);
			animator.SetBool ("IdleEye", false);
			yield return new WaitForSeconds (2f);
			idle = false;
			

		}

		if (idle == false) {
			animator.SetBool ("Idle", false);
			animator.SetBool ("IdleEye", true);
			yield return new WaitForSeconds (0.5f);
			idle = true;

		}


	}

	IEnumerator JumpingTimer(){
		yield return new WaitForSeconds (3f);
		jumped = false;
		StopCoroutine ("JumpingTimer");

	}



	void OnCollisionEnter2D (Collision2D other){

		if (other.transform.name == "YinPlayer" || other.transform.name == "YangPlayer") {

			gmScript.Death = true;
		}
		if(other.transform.tag == "Floor"){
			onGround = true;
			animator.SetBool ("Landing", true);
			animator.SetBool ("TakeOff", false);
			if (player.x > transform.position.x) {
				transform.localScale = new Vector2 (-1.5f, transform.localScale.y);
			}
			if (player.x < transform.position.x) {
				transform.localScale = new Vector2 (1.5f, transform.localScale.y);
			}

		}

	}
	void OnCollisionStay2D (Collision2D other){

		if(other.transform.tag == "Floor"){
			animator.SetBool ("Landing", false);
			//animator.SetBool ("TakeOff", false);

		}

	}
	void OnCollisionExit2D (Collision2D other){

		if(other.transform.tag == "Floor"){
			//onGround = false;
		}

	}
}
