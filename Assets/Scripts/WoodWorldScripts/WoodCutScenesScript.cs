﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodCutScenesScript : MonoBehaviour {

	[SerializeField]
	private Transform yinPos;
	[SerializeField]
	private Transform yangPos;

	private Vector3 middle;
	private float minSizeY;
	private float maxSizeY;

	private bool playingCutscene;

	public bool PlayingCutscene {
		get {
			return playingCutscene;
		}
	}

	[SerializeField]
	private float speed;


	private bool setCam1;
	private bool moveCam1;
	[SerializeField]
	private Transform start1;
	[SerializeField]
	private Transform end1;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		middle = (yinPos.position + yangPos.position) / 2f;
		minSizeY = middle.y-5;
		maxSizeY = middle.y+5;
		StartCutscene ();
	}

	void StartCutscene(){
		
		end1.transform.position = new Vector3 (middle.x,middle.y,transform.position.z);

		if(setCam1 == false){
			playingCutscene = true;
			transform.position = new Vector3 (start1.transform.position.x,start1.transform.position.y, transform.position.z);
			setCam1 = true;
		}

		if(moveCam1 == false){
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (middle.x, Mathf.Clamp (transform.position.y, minSizeY, maxSizeY),transform.position.z), 3 * Time.deltaTime);
		}
			
		if(transform.position.y <= end1.position.y+5.1){
			moveCam1 = true;
			playingCutscene = false;
		}
	}
}
