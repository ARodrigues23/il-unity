﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradientFollowScripts : MonoBehaviour {

	private Transform cameraPos;

	// Use this for initialization
	void Start () {
		cameraPos = GameObject.Find ("Main Camera").GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position = new Vector3 (cameraPos.transform.position.x,cameraPos.transform.position.y,cameraPos.transform.position.z+30f);
	}
}
