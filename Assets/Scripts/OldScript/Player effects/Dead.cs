﻿using UnityEngine;
using System.Collections;

public class Dead : MonoBehaviour {

	public LevelManager manager;

	private Animator anim;


	// Use this for initialization
	void Start () {
		

	

	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag=="Player"){

			manager.RespawnPlayer();

		}

	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag == "Player") {


			manager.RespawnPlayer ();


		}
	}


	void Restart(){

		//Application.LoadLevel (Application.loadedLevel);

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.R))
			PlayerPrefs.SetInt("checkpoint", 0);
	}
}

