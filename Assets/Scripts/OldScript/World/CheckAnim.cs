﻿using UnityEngine;
using System.Collections;

public class CheckAnim : MonoBehaviour {

	public Animator anim;



	// Use this for initialization
	void Start () {
		anim.enabled = false;
	
	}

	void OnTriggerEnter2D (Collider2D other){
		anim.enabled = true;
	
		GetComponent<Animator>().Play("Checpoint");

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
