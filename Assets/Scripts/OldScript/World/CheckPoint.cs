﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	public LevelManager levelManager;

	// Use this for initialization
	void Start () {

		levelManager = FindObjectOfType<LevelManager> ();

	}
	
	// Update is called once per frame
	void Update () {
		

	
	}

	void OnTriggerEnter2D (Collider2D other){

		if (other.name == "yin" || other.name == "yang") {

			levelManager.currentCheckpoint = gameObject;
			Debug.Log ("Activeted CheckPoint " + transform.position);
			Debug.Log (levelManager.currentCheckpoint);
		}

	}
}
