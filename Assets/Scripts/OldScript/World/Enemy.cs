﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float velocity = 1f;
	public float xmin;
	public float xmax;
	private int rotation;
	private bool once;
	// Use this for initialization
	void Start () {

		once = true;
		rotation = 0;
	}

	// Update is called once per frame
	void Update () {

		Change ();

		GetComponent<Rigidbody2D>().velocity = new Vector2 (velocity, GetComponent<Rigidbody2D>().velocity.y);

	}

	void Change (){
		if (once&&(transform.position.x >= xmax || transform.position.x <= xmin)) {

			once = false;
			rotation = rotation + 180;
			transform.eulerAngles = new Vector2 (0, rotation);
			velocity = -velocity;
		}

		else{
			once = true;

		}
	}
}
