﻿using UnityEngine;
using System.Collections;

public class TrampolineScript : MonoBehaviour {
	public float springForce = 1000;
	private Collision2D collision;
	private bool bouncing = false;
	public GameObject yin, yang;
	public float forcex;
	public float forcey;


	private void Start (){
		yin = GameObject.Find("yin");
		yang= GameObject.Find("yang");
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (!bouncing) {
			bouncing = true;
			collision = coll;

		}
	}

	void FixedUpdate () {
		if (bouncing) {
			Rigidbody2D rbyin = yin.GetComponent<Rigidbody2D>();
			Rigidbody2D rbyang = yang.GetComponent<Rigidbody2D>();
			rbyin.velocity = new Vector3 (forcex, forcey, 0);
			rbyin.AddForce (new Vector2 (0f, springForce));
			rbyang.velocity = new Vector3 (forcex, forcey, 0);
			rbyang.AddForce (new Vector2 (0f, springForce));


			bouncing = false;
		}

	}
}
