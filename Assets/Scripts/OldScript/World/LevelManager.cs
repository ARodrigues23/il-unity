﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckpoint;

	public GameObject pCheck;
	public GameObject Cameracutscene;


	private PlayerController player;
	private PlayerController2 player2;

	private PedraRespawn pedra;
	public TestCam camera;
	public Transform Yin, Yang;
	public Canvas CutsceneCanvas;

	// Use this for initialization
	void Start () {

		// pedra
		pedra = FindObjectOfType<PedraRespawn> ();


		player = FindObjectOfType<PlayerController> ();
		player2 = FindObjectOfType<PlayerController2> ();
	}
	
	// Update is called once per frame
	void Update () {


	
	}

	public void RespawnPlayer (){


		Debug.Log("Player Respawn");



		player.transform.position = currentCheckpoint.transform.position;
		player2.transform.position = currentCheckpoint.transform.position;


	}

	public void RespawnPedra (){
	
	
		//pedra volta ao sitio quando os players morrem
		pCheck.transform.position = pedra.transform.position;
	
	}
	public void NoCut (){
	
		Cameracutscene.GetComponent<Animator> ().enabled = false;
		camera.enabled = true;
		CutsceneCanvas.enabled = false;
		Yang.GetComponent<PlayerController2> ().enabled = true;
		Yin.GetComponent<PlayerController> ().enabled = true;
	
	
	}
		
	

}
