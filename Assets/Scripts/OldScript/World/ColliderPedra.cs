﻿using UnityEngine;
using System.Collections;

public class ColliderPedra : MonoBehaviour {

	private Collision2D collision;
	public GameObject pedra;


	// Use this for initialization
	private void Start () {

		pedra = GameObject.Find ("Pedra");
		pedra.GetComponent<Rigidbody2D> ().isKinematic = true;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll){
		
		if (coll.tag == "Player") {
			pedra.GetComponent<Rigidbody2D> ().isKinematic = false; 
		}
	}
}
