﻿using UnityEngine;
using System.Collections;

public class TheEnd : MonoBehaviour {

	public GameObject endmenu;

	public PlayerController player;

	public GameObject fade;
	public GameObject yin;
	public GameObject yang;


	// Use this for initialization
	void Start () {
		
		endmenu.SetActive (false);
		player = FindObjectOfType<PlayerController> ();


	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.name == "yin" || other.name == "yang") {
			endmenu.SetActive (true);
			fade.SetActive (true);
			yin.SetActive (false);
			yang.SetActive (false);



		}
	}
}