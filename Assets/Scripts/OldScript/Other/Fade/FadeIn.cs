﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {
    public Texture2D fadeOut;
    public float fadeSpeed = 0.8f;

    private int drawdepth = -1000; // ordem na hierchy
    private float alpha = 1.0f; // textura da alpha endtre 1 e 0
    private int fadeDir = -1; // direção do fade- 1n= -1, out = 1

    void OnGUI()
    {
        //fade in out o valor alpha usando direção, velocidade, e time.deltatime para converter a operação ems egundos

        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        // forcar o numero entre 1 e 0 porque gui.color usa valores entre 0 e 1
        alpha = Mathf.Clamp01(alpha);

        // por cor do GUI
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha); // set the alpha value
        GUI.depth = drawdepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOut);// textura enche o ecra todo

    }
    //fade direção in -1, out 1

    public float BeginFade (int direction)
    {
        fadeDir = direction;
        return (fadeSpeed); // return a variavel fadespeed para que seja facil por o tempo de aplication.loadlevel

    }

    //On LevelWasLoaded, faz o fade

    void OnLevelWasLoaded()
    {
        //alpha =1; // usa isto se alpha not é 1 em default
        BeginFade(-1); // chama o fade
    }
}
