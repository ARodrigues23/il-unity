﻿using UnityEngine;
using System.Collections;

public class TutColli : MonoBehaviour {

	public Animator Tutorial;
	public bool animating;
	public bool OneTime;
	public string selecttut;


	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter2D (Collider2D cut){

		if (cut.tag == "Player" && OneTime == false) {

			OneTime = true;
			Tutorial.SetTrigger (selecttut);
			StartCoroutine ("OnCollide");


		}

	}

	IEnumerator OnCollide(){


		animating = true;




		yield return new WaitForSeconds(15);


		animating = false;





	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
