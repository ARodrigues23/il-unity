﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioClip hitSound;
	public AudioClip menuSound;

	public AudioClip[] randomSound;

	public AudioSource backGroundaudio;
	public AudioSource effectsAudio;

	public AudioClip backGroundMusic;

	public static AudioManager instance;

	// Use this for initialization
	void Start () {

		instance = this;
		backGroundaudio.clip = backGroundMusic;
		backGroundaudio.Play ();

	
	}
	
	// Update is called once per frame
	public void Update () {
	
	}

	public void PlaySound(){
		
		effectsAudio.clip = hitSound;
		effectsAudio.Play ();
	
	}

	public void PlayMenuSound(){
		
		effectsAudio.clip = menuSound;
		effectsAudio.Play ();
	}

	public void PlayRandomSound(){
		
		int random = Random.Range (0, randomSound.Length - 1);
		effectsAudio.clip = randomSound [random];
		effectsAudio.Play ();

	
	}
}
