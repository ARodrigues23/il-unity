﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {

	public bool blockmovement;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BlockMovemente(){
		blockmovement = true;
		
	}

	public void ReleaseMovement(){
		blockmovement = false;
	}

	public bool CheckMovement(){
		if (blockmovement == true) {
			return false;
		
		} else
			return true;

	}
}
