﻿using UnityEngine;
using System.Collections;

public class Camerafollow : MonoBehaviour {
	
	[SerializeField]
	private float xMax;

	[SerializeField]
	public float yMax;

	[SerializeField]
	private float xMin;

	[SerializeField]
	public float yMin;

	private Transform target;

	void Start(){

		target = GameObject.Find ("yin").transform;
	
	}

	void LateUpdate(){

		transform.position = new Vector3 (Mathf.Clamp (target.position.x, xMin, xMax), Mathf.Clamp (target.position.y, yMin + 10, yMax + 10), transform.position.z);
	
	}
}