﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour
{
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool grounded;

	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatIsWall;
	private bool wall;

	private bool doubleJump;

	private bool wallJump;

    public float jumpHeight;


	public float speed = 5;
	public string axis = "Vertical";

	public float x;


    // Use this for initialization
    void Start()
    {

    }

	void FixedUpdate(){

		wall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);

	}

    // Update is called once per frame
    void Update()
    {

		if (grounded) {
			doubleJump = false;
			wallJump = false;

		}



		if (Input.GetKeyDown (KeyCode.UpArrow) && grounded) {

			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);

		}

		if (Input.GetKeyDown (KeyCode.UpArrow) && !doubleJump && !grounded) {

			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
			doubleJump = true;

		}

		if (Input.GetKeyDown (KeyCode.UpArrow) && !wallJump && wall) {


			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
			wallJump = true;

		}

    }

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.layer == 11 && !grounded) {
			x = transform.position.x;
			GetComponent<Rigidbody2D> ().isKinematic = true;
			GetComponent<Rigidbody2D> ().gravityScale = 0;

		}
	}

}
