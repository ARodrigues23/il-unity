﻿using UnityEngine;
using System.Collections;

public class PlayerController2 : MonoBehaviour {

	public float movespeed;
	public float jumpHeight;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool grounded2;

	private bool doubleJumped;

	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatIsWall;
	private bool wall;

	private bool wallJump;

	float timer = 0.1f;
	public float speed;
	public float max;
	public float slideInit;
	float slide;

	public float distancia;
	public Transform yin;
	private Vector2 blockedposition;
	public bool limite;

	public Vector2 checkposition;

	private Manager manager;
	public float force = 0.1f;

	private Animator anim1;







	// Use this for initialization
	void Start () {


		anim1 = GetComponent<Animator> ();

		manager = GameObject.Find ("Main Camera").GetComponent<Manager> ();

		if (PlayerPrefs.GetInt("checkpoint")==1) {

			transform.position = checkposition;
		}



	}



	void FixedUpdate(){

		wall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);
		grounded2 = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);

		HandleLayers ();
	}

	// Update is called once per frame
	void Update () {

		if (Time.timeScale != 0) {

			if (yin.GetComponent<PlayerController> ().grounded == false && grounded2 == true) {
				//Debug.Log ("Entrou yang");
				if (Mathf.Abs (transform.position.y - yin.transform.position.y) >= 5) {
					//Debug.Log ("Entrou yang 1");
					if (transform.position.y > yin.transform.position.y && GetComponent<DistanceJoint2D> ().distance == 10) {
						//Debug.Log ("Entrou yang 2");
						Pendulo ();
						//manager.GetComponent<Manager> ().BlockMovemente ();
				
					}
					
				}
			} else if (yin.GetComponent<PlayerController> ().grounded == true || grounded2 == false) {
				GetComponent<Rigidbody2D> ().isKinematic = false;
				//manager.GetComponent<Manager> ().ReleaseMovement ();
				GetComponent<Rigidbody2D> ().mass =1;
			}
			if (manager.CheckMovement ()) {

				if (grounded2) {
					doubleJumped = false;
					wallJump = false;
					anim1.ResetTrigger ("Jump1");
					anim1.SetBool ("Land1", false);
					anim1.ResetTrigger ("Djump1");

				}

				if (GetComponent<Rigidbody2D> ().velocity.y < 0) {

					anim1.SetBool ("Land1", true);
				}


				if (Input.GetKeyDown (KeyCode.Escape))
					Application.Quit ();


				if (Input.GetKeyDown (KeyCode.UpArrow) && grounded2) {
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);

					anim1.SetTrigger ("Jump1");

				}

				if (Input.GetKeyDown (KeyCode.UpArrow) && !doubleJumped && !grounded2) {

					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
					doubleJumped = true;

					anim1.SetTrigger ("Djump1");
				}

				if (Input.GetKeyDown (KeyCode.UpArrow) && !wallJump && wall) {

					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
					wallJump = true;

				}

				if (Input.GetKey (KeyCode.RightArrow)) {

					transform.Translate (Vector2.right * movespeed * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 0);
					Changespeed ();

				} else if (Input.GetKey (KeyCode.LeftArrow)) {

					transform.Translate (Vector2.right * movespeed * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 180);
					Changespeed ();


				}

				if (!Input.GetKey (KeyCode.RightArrow) && !Input.GetKey (KeyCode.LeftArrow)) {
					Slidespeed ();
				}



				transform.Translate (Vector2.right * 2f * Time.deltaTime * speed);

			}
		}
		anim1.SetFloat ("Speed1", Mathf.Abs (GetComponent<PlayerController2> ().speed));
	}
	void Changespeed () {

		timer = timer - Time.deltaTime;

		if (timer < 0) {
			Addspeed (1);
			timer = 0.1f;
		}
	}
	void Addspeed (float value) {

		speed = speed+value;

		if (speed > max)
			speed = max;
	}

	void Removespeed (float value){

		speed = speed - value;

		if (speed < 0)
			speed = 0;
	}

	void Slidespeed () {

		slide = slide - Time.deltaTime;
		//Debug.Log ("Sliding");

		if (slide <= 0) {
			Removespeed (2);
			slide = slideInit;
		}

	}

	void VerificaDistancia (){

		if (Vector3.Distance (transform.position, yin.position) >= distancia) {

			if (limite == false) {
				limite = true;
				Block ();
			}

			Debug.Log ("Far");
			transform.position = new Vector2 (blockedposition.x,blockedposition.y);


		} else
			limite = false;


	}

	void Block(){

		blockedposition = transform.position;

		blockedposition.x = transform.position.x;

		blockedposition.y = transform.position.y;

	}

	void Pendulo(){
		//ebug.Log ("Hello2");

		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.D)) {
			yin.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Input.GetAxis ("Yin horizontal") * 1.5f, 0), ForceMode2D.Impulse);
		}

		//GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<Rigidbody2D> ().mass = 7;
	}

	private void HandleLayers(){

		if (!grounded2) {

			anim1.SetLayerWeight (1, 1);
		} 
		else {

			anim1.SetLayerWeight (1, 0);
		}

	}





}