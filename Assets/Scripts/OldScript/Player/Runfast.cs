﻿using UnityEngine;
using System.Collections;

public class Runfast : MonoBehaviour {

	float timer = 0.1f;
	public float speed;
	public float max;
	public float slideInit;
	float slide;

	public float distancia;
	public Transform yin;
	private Vector2 blockedposition;
	public bool limite;

	public Vector2 checkposition;



	// Use this for initialization
	void Start () {

		if (PlayerPrefs.GetInt("checkpoint")==1) {

			transform.position = checkposition;
		}

	
	}

	void Changespeed () {
	
		timer = timer - Time.deltaTime;

		if (timer < 0) {
			Addspeed (1);
			timer = 0.1f;
		}



	}
	// Update is called once per frame
	void Update () {



		Movement ();
	
	}
	void Addspeed (float value) {

		speed = speed+value;

		if (speed > max)
			speed = max;

	}

	void Removespeed (float value){
	
		speed = speed - value;

		if (speed < 0)
			speed = 0;
	}
	void Movement(){

		if (Input.GetKey (KeyCode.RightArrow)) {
			
			Changespeed ();

			transform.eulerAngles = new Vector2 (0, 0);

		} else if (Input.GetKey (KeyCode.LeftArrow)) {

			Changespeed ();

			transform.eulerAngles = new Vector2 (0, 180);

		}

		if (!Input.GetKey (KeyCode.RightArrow) && !Input.GetKey (KeyCode.LeftArrow)) {
			Slidespeed();
		}

		transform.Translate (Vector2.right * 2f * Time.deltaTime * speed);
	}
	void Slidespeed () {

		slide = slide - Time.deltaTime;
		Debug.Log ("Sliding");

		if (slide <= 0) {
			Removespeed (2);
			slide = slideInit;
		}



	}



}