﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float movespeed;
	public float jumpHeight;
	public bool cjumpy;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool grounded;

	private bool doubleJumped;

	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatIsWall;
	private bool wall;


	private bool wallJump;

	float timer = 0.1f;
	public float speed;
	public float max;
	public float slideInit;
	float slide;

	public float distancia;
	public Transform yang;
	private Vector2 blockedposition;
	public bool limite;

	public Vector2 checkposition;

	private Manager manager;
	public float force = 0.1f;

	private Animator anim;

	public bool sliding = false;







	// Use this for initialization
	void Start () {

		sliding = false;

		anim = GetComponent<Animator> ();

		manager = GameObject.Find ("Main Camera").GetComponent<Manager> ();

		if (PlayerPrefs.GetInt("checkpoint")==1) {

			transform.position = checkposition;
		}
	
	}



	void FixedUpdate(){
		
		wall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);

		HandleLayers ();
	}
	
	// Update is called once per frame
	void Update () {



		if (Time.timeScale != 0) {

			if (yang.GetComponent<PlayerController2> ().grounded2 == false && grounded == true) {
				//Debug.Log ("Entrou yin");
				if (Mathf.Abs (transform.position.y - yang.transform.position.y) >= 5) {
					//Debug.Log ("Entrou yin 1");
					if (transform.position.y > yang.transform.position.y && yang.GetComponent<DistanceJoint2D> ().distance == 10) {
						//Debug.Log ("Entrou yin 2");
						Pendulo ();
						//manager.GetComponent<Manager> ().BlockMovemente ();
					}
				}
			} else if (yang.GetComponent<PlayerController2> ().grounded2 == true || grounded == false) {
				//GetComponent<Rigidbody2D> ().isKinematic = false;
				//manager.GetComponent<Manager> ().ReleaseMovement ();
				GetComponent<Rigidbody2D> ().mass = 1;

			}
			if (manager.CheckMovement ()) {
			
				if (grounded) {
					doubleJumped = false;
					wallJump = false;
					anim.ResetTrigger ("Jump");
					anim.ResetTrigger ("Djump");
					anim.SetBool ("Landing", false);


		
					} 

				if (GetComponent<Rigidbody2D> ().velocity.y < 0) {
				
					anim.SetBool ("Landing", true);
				
				}


				if (Input.GetKeyDown (KeyCode.W) && grounded) {

					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
					anim.SetTrigger ("Jump");
	
		
				} 

				if (Input.GetKeyDown (KeyCode.W) && !doubleJumped && !grounded) {


					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
					doubleJumped = true;
					anim.SetTrigger ("Djump");


				}

				if (Input.GetKeyDown (KeyCode.W) && !wallJump && wall) {

					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpHeight);
					wallJump = true;

				}

				if (Input.GetKey (KeyCode.D)) {

				

					transform.Translate (Vector2.right * movespeed * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 0);
					Changespeed ();



				

				} else if (Input.GetKey (KeyCode.A)) {
	

					transform.Translate (Vector2.right * movespeed * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 180);
					Changespeed ();
		
				
				}

				if (!Input.GetKey (KeyCode.D) && !Input.GetKey (KeyCode.A)) {
					Slidespeed ();
				}



				transform.Translate (Vector2.right * 2f * Time.deltaTime * speed);
	
			}


		}
		anim.SetFloat ("speed", Mathf.Abs (GetComponent<PlayerController> ().speed));


	
	}
	
	void Changespeed () {

		timer = timer - Time.deltaTime;

		if (timer < 0) {
			Addspeed (1);
			timer = 0.1f;
		}
}
	void Addspeed (float value) {

		speed = speed+value;

		if (speed > max)
			speed = max;
}

	void Removespeed (float value){

		speed = speed - value;

		if (speed < 0)
			speed = 0;
}

	void Slidespeed () {

		slide = slide - Time.deltaTime;
		//Debug.Log ("Sliding");

		if (slide <= 0) {
			Removespeed (2);
			slide = slideInit;
		}



}

	void VerificaDistancia (){

		if (Vector3.Distance (transform.position, yang.position) >= distancia) {

			if (limite == false) {
				limite = true;
				Block ();
			}

			//Debug.Log ("Far");
			transform.position = new Vector2 (blockedposition.x,blockedposition.y);


		} else
			limite = false;


	}


	void Block(){

		blockedposition = transform.position;

		blockedposition.x = transform.position.x;

		blockedposition.y = transform.position.y;

	}

	void Pendulo(){

		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow)) {
			yang.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Input.GetAxis ("Horizontal") * 1.5f, 0), ForceMode2D.Impulse);
		}

		//GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<Rigidbody2D> ().mass = 7;
	
	}

	private void HandleLayers(){

		if (!grounded) {
		
			anim.SetLayerWeight (1, 1);
		} 
		else {

			anim.SetLayerWeight (1, 0);
		}

	}



}
