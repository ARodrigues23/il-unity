﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EticLogoScript : MonoBehaviour {

	[SerializeField]
	private SpriteRenderer blackImage;

	private bool goBlack;
	private bool goWhite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		FadeIn ();
		FadeOut ();
		StartCoroutine ("GoToMainMenu",0f);
	}

	void FadeIn(){
		if(goWhite == true){
			blackImage.color = new Color (0f, 0f, 0f, blackImage.color.a - 0.015f);
			if(blackImage.color.a <= 0f){
				blackImage.color = new Color (0f,0f,0f,0f);
			}
		}

	}
	void FadeOut(){
		if(goBlack == true){
			blackImage.color = new Color (0f, 0f, 0f, blackImage.color.a + 0.03f);
			if(blackImage.color.a >= 1f){
				blackImage.color = new Color (0f,0f,0f,1f);
			}
		}

	}

	IEnumerator GoToMainMenu () {
		goWhite = true;
		yield return new WaitForSeconds (3.5f);
		goBlack = true;
		yield return new WaitForSeconds (3.5f);
		SceneManager.LoadScene ("MainMenu",LoadSceneMode.Single);
	}
}
